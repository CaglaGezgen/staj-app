const express = require ("express");
const bodyParser = require('body-parser');
const  mongoose = require('mongoose');  

//connection to mongodb via mlab
mongoose.connect('mongodb://cagla:zorro909.@ds249137.mlab.com:49137/intern-app', {useNewUrlParser: true}); 

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log("connected to mongodb");
}); 

const app = express();

//global middlewares
app.use(bodyParser.json())

//middlewares
app.use("/api", require('./src/routes'))

const port = 3000;
app.listen(port,()=>{console.log("server listening")});