const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

  email: {
    type: String,
    required:true,
    trim:true,
    minlength:6,
    unique:true
  },
  password: {
    type:String,
    required:true,
    min:6,
    max:1024
  },
  userName: {
    type:String,
    required:true,
    min:5,
    max:1024,
    unique:true
   }
  

}, {timestamps:true});

const User = mongoose.model("User", userSchema);

module.exports = User;