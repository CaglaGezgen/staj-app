const mongoose = require('mongoose');

const Product = mongoose.model('Product',productSchema);
   
const productSchema = new mongoose.Schema({
    productName : {
       type:String,
       required:true,
       min:2,
       max:1024,
      unique:true
    },
    productBrand : {
        type:String,
        required:true,
        max:1024,
        uppercase:true
    },
     price : {
        type:String,
        required:true,
        min:2,
        max:1024
       },
       imageUrl: {
        type:String,
        default:"https://picsum.photos/200"
       },
       quantity :{
           type:Number,
           required:true  
       },
       status : {
           type:Boolean
       }


}, {timestamps:true});


module.exports = Product;



