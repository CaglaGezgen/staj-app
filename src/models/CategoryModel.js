const mongoose = require('mongoose');

const Category = mongoose.model('Category',categorySchema);
   

const categorySchema = new mongoose.Schema({
 categoryName : {
    type:String,
    required:true,
    min:2,
    max:1024,
    trim:true,
    unique:true,
    type: Schema.Types.ObjectId,
    ref: 'Product'
 },
 description : {
    type:String,
    required:true,
    min:2,
    max:1024,
    trim:true,
    type: Schema.Types.ObjectId,
    ref: 'Product'
 }
 
}, {timestamps:true});

module.exports = mongoose.model('Category',categorySchema);