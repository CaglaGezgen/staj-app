const router = require('express').Router();

const usersRoute = require('./usersRoute');
const productsRoute = require('./productsRoute');
const categoriesRoute = require('./categoriesRoute');

//www.server.com/api
router.use("/users",usersRoute);
router.use("/products",productsRoute);
router.use("/categories",categoriesRoute);

module.exports = router;