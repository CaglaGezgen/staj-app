const router = require('express').Router();
const categoriesController =require('../controllers/categoriesController');

//wwww.server.com/api/category
//Category Operations
router.post("/",categoriesController.createCategory);
router.get("/categories/:categoryId",categoriesController.getCategory);
router.put("/categories/:categoryId",categoriesController.updateCategory);
router.delete("/categories/:categoryId",categoriesController.deleteCategory);

//Shows the products according to the categoryId
router.get("/categories/:categoryId/products/:productId",categoriesController.getProductByCategoryId);
//Returns the number of categories
router.get("/categories/count",categoriesController.getCategoriesCount)
//Returns a list of categories
router.get("/categories",categoriesController.getCategoriesList);


module.exports = router;