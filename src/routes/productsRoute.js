const router = require('express').Router();
const productsController =require('../controllers/productsController');

//wwww.server.com/api/users
//Product Operations
router.post("/",productsController.createProduct);
router.get("/products/:productId",productsController.getProduct);
router.put("/products/:productId",productsController.updateProduct);
router.delete("/products/:productId",productsController.deleteProduct);

//Product Operations for Product List
router.get("/products/:productId/productDetail",productsController.getProductDetailByProductId);
//Returns the number of products
router.get("/products/count",productsController.getProductsCount);
//Returns the product List
router.get("/products",productsController.getProductsList);

//zaman kalırsa grafik yaparım 
//router.get("/products/graph",productsController.getProductGraph);


module.exports = router;