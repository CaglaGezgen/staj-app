const router = require('express').Router();
const usersController =require('../controllers/usersController');

//wwww.server.com/api/users
//User Operations
router.post("/",usersController.createUser);
router.get("/users/:userId",usersController.getUser);
router.put("/users/:userId",usersController.updateUser);
router.delete("/users/:userId",usersController.deleteUser);

//Product Operations by UserId
router.get("/users/:userId/products/:productId",usersController.getUserProductByUserId);
router.get("/users/:userId/categories/:categoryId",usersController.getUserCategoryByUserId);




//login and sign up
router.post("/signUp",usersController.signUp);
router.post("/signIn",usersController.signIn);
router.post("/logOut",usersController.logOut);



module.exports = router;