const User = require('../models/UserModel');
const bcrypt = require('bcryptjs');


async function signUp(req, res, next) {
  // get new user info from request body
  const {email, password, passwordConfirm} = req.body;
  if(password !== passwordConfirm){
    console.log("password does not match");
    return res.status(400).send("Password does not match.")
  }

  try {
    // check user already exists
    const foundUser = await User.findOne({ email });

    // if user exists
    if(foundUser){
      return res.status(400).send("User already exists.");
    }

    // user not exists, create new one
    // hash password
    bcrypt.hash(password, 10, async (err, hashedPassword) => {
      // Store hash in your password DB.
      if(err){
        console.log("bcrypt hash function", err);
        return res.status(500).send("Somethings happened. Server error")
      }
      
      try {
        const newUser = new User({
          email,
          password:hashedPassword
        });
        const createdUser = await newUser.save();
        // user created send result
        return res.status(201).send({user:{email:createdUser.email, userId:createdUser._id}});
      } catch (error) {
        console.log("saving user to db", error);
        res.status(500).send("Somethings happened. Server error")
      }
      
    });
    
  } catch (error) {
    console.log("sign up function", error);
    res.status(500).send("Somethings happened. Server error")
  }

}

function createUser(req,res,next)
{
    console.log("create User Function")
    res.status(201).send({newUser:"Cagla"});
}

function getUser(req,res,next){
    console.log("get User Function")
    res.status(200).send({User:""});
}

function deleteUser(req,res,next){
    console.log("delete UserdeleteUser Function")
    res.status(200).send({UserdeleteUser:"laptop"});
}

 
function updateUser(req,res,next)
{
    console.log("update User function");
    res.status(200).send({User:""});
}

function getUserProductByUserId(req,res,next)
{
    const userId=req.params.userId;
    const postId = req.params.ProductId;

    console.log("getUserProductByUserId");
    res.status(200).send({ProductUser:""})
}
function getUserCategoryByUserId(req,res,next)
{
    console.log("getUserCategoryByUserId");
    res.status(200).send({CategoryUser:""})
}

function getMe(req,res,next){
    
}
function signIn(req,res,next){

}
function logOut(req,res,next){

}

module.exports = {
    createUser,
    getUser,
    updateUser,
    deleteUser,
    getUserProductByUserId,
    getUserCategoryByUserId,
    signUp,
    getMe,
    signIn,
    logOut
 }


