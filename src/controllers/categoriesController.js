function createCategory(req,res,next){
    console.log("create Category Function")
    res.status(201).send({newCategory:"laptop"});

}
function getCategory(req,res,next){
    console.log("get Category Function")
    res.status(200).send({Category:"laptop"});
}

function deleteCategory(req,res,next){
    console.log("delete Category Function")
    res.status(200).send({Category:"laptop"});
}

function updateCategory(req,res,next){
    console.log("update Category function");
    res.status(200).send({Category:"laptop"});
}
function getProductByCategoryId(req,res,next)
{

}
function getCategoriesCount(req,res,next) 
{

}
function getCategoriesList(req,res,next){

}
module.exports = {
    createCategory,
    getCategory,
    deleteCategory,
    updateCategory,
    getProductByCategoryId,
    getCategoriesCount,
    getCategoriesList
}
